﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DeviceMang.Models;

namespace DeviceMang.Controllers
{
    public class UserToDevicesController : Controller
    {
        private UsersDevicesDB db = new UsersDevicesDB();

        // GET: UserToDevices
        public ActionResult Index()
        {
            var userToDevices = db.UserToDevices.Include(u => u.Device).Include(u => u.User);
            return View(userToDevices.ToList());
        }

        // GET: UserToDevices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserToDevice userToDevice = db.UserToDevices.Find(id);
            if (userToDevice == null)
            {
                return HttpNotFound();
            }
            return View(userToDevice);
        }

        // GET: UserToDevices/Create
        public ActionResult Create()
        {
            ViewBag.IdDevice = new SelectList(db.Devices, "IdDevice", "Name");
            ViewBag.IdUser = new SelectList(db.Users, "IdUser", "Name");
            return View();
        }

        // POST: UserToDevices/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdDevice,IdUser")] UserToDevice userToDevice)
        {
            if (ModelState.IsValid)
            {
                db.UserToDevices.Add(userToDevice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdDevice = new SelectList(db.Devices, "IdDevice", "Name", userToDevice.IdDevice);
            ViewBag.IdUser = new SelectList(db.Users, "IdUser", "Name", userToDevice.IdUser);
            return View(userToDevice);
        }

        // GET: UserToDevices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserToDevice userToDevice = db.UserToDevices.Find(id);
            if (userToDevice == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdDevice = new SelectList(db.Devices, "IdDevice", "Name", userToDevice.IdDevice);
            ViewBag.IdUser = new SelectList(db.Users, "IdUser", "Name", userToDevice.IdUser);
            return View(userToDevice);
        }

        // POST: UserToDevices/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdDevice,IdUser")] UserToDevice userToDevice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userToDevice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdDevice = new SelectList(db.Devices, "IdDevice", "Name", userToDevice.IdDevice);
            ViewBag.IdUser = new SelectList(db.Users, "IdUser", "Name", userToDevice.IdUser);
            return View(userToDevice);
        }

        // GET: UserToDevices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserToDevice userToDevice = db.UserToDevices.Find(id);
            if (userToDevice == null)
            {
                return HttpNotFound();
            }
            return View(userToDevice);
        }

        // POST: UserToDevices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserToDevice userToDevice = db.UserToDevices.Find(id);
            db.UserToDevices.Remove(userToDevice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
