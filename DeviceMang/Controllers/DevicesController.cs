﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DeviceMang.Models;

namespace DeviceMang.Controllers
{
    public class DevicesController : Controller
    {
        private UsersDevicesDB db = new UsersDevicesDB();

        // GET: Devices
        public ActionResult Index()
        {
            return View(db.Devices.ToList());
        }

        // GET: Devices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // GET: Devices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Devices/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdDevice,Name,Type,Manufacturer,Os,OSversion,Proccesor,RAM")] Device device)
        {
            if (device.Name != null && device.Type != null && device.Manufacturer != null && device.Os != null && device.OSversion != null)
            {
                if (ModelState.IsValid)
                {
                    db.Devices.Add(device);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else Response.Write("<div class='alert alert-danger' role='alert'> Please try again! </div>");
            return View(device);
        }

        // GET: Devices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // POST: Devices/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdDevice,Name,Type,Manufacturer,Os,OSversion,Proccesor,RAM")] Device device)
        {
            if (device.Name != null && device.Type != null && device.Manufacturer != null && device.Os != null && device.OSversion != null)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(device).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else Response.Write("<div class='alert alert-danger' role='alert'> Please try again! </div>");
            return View(device);
        }

        // GET: Devices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // POST: Devices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Device device = db.Devices.Find(id);
            var FindRows = db.UserToDevices.Where(x => x.IdDevice == id);
            foreach (var Row in FindRows)
            {
                db.UserToDevices.Remove(Row);
            }
            db.Devices.Remove(device);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
