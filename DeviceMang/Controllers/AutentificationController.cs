﻿using DeviceMang.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DeviceMang.Controllers
{
    public class AutentificationController : Controller
    {
        private UsersDevicesDB db = new UsersDevicesDB();
        // GET:Autentification/Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login([Bind(Include = "Email,Password")] User user)
        {
            if (user.Email != null && user.Password != null)
            {
                User dbuser = db.Users.SingleOrDefault(x => x.Email == user.Email);
                if ((dbuser != null) && (dbuser.Password == user.Password))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.Email, false);
                }
                else Response.Write("<div class='alert alert-danger' role='alert'> Please try again! </div>");
                return View();
            }
            else
            {
                Response.Write("<div class='alert alert-danger' role='alert'> Please try again! </div>");
                return View();
            }

        }


        public ActionResult SignUp()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp([Bind(Include = "IdUser,Name,Role,Location,Password,Email")] User user)
        {
            if (user.Name != null && user.Role != null && user.Password != null && user.Email != null && user.Location!=null)
            {
                if (ModelState.IsValid)
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Login");
                }
            }
            else Response.Write("<div class='alert alert-danger' role='alert'> Please try again! </div>"); ;

            return View(user);
        }
        public ActionResult Signu()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

    }


}